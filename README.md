## Extensions

* Auto Close Tag - Jun Han
* Auto Rename Tag - Jun Han
* Bracket Pair Colorizer - CoenraadS
* DotENV - mikestead
* Duplicate action - mrmlnc
* ES Lint - Dirk Baeumer
* GitLens - GitKraken
* ES7 React/Redux/GraphQL/React-Native snippets - dsznajder
* Quokka.js - Wallaby.js
* Reactjs code snippets - charalampos karypidis
* Tailwind CSS IntelliSense - Tailwind Labs
* Material Icon Theme - Philipp Kief
* Path Autocomplete - Mihai Vilcu
* Prettier - Code formatter - Esben Petersen
* Markdown Preview Enhanced - Yiyi Wang
* Live Server - ritwik dey
* vscode-icons - VSCode Icons Team
* Turbo Console Log - ChakrounAnas
* expressjs - amandeepmittal

## Laravel Extensions
* Laravel Blade formatter - Shuhei Hayashibara
* Laravel Blade Snippets - Winnie Lin
* Laravel Blade Spacer - Austen Cameron
* Laravel Extra Intellisense - amir
* Laravel goto view - codingyu
* aravel intellisense - Mohamed Benhida
* laravel-goto-controller - stef-k

## Python Extensions
* fastapi-snippets -Samuel Adekoya
